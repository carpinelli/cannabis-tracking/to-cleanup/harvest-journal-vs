#!/usr/bin/env python3

"""Enter waste into Harvest Journal using a CSV report from Metrc."""

import datetime
from datetime import datetime
import logging
from pathlib import Path
import random
import traceback
import time

import pandas as pd
import pyautogui
import pyperclip

from modules import harvest_journal
from modules import helpers
from modules import ht_pyautogui
from modules import tgs

from IPython.core.debugger import set_trace


# # # Globals / Logger # # #

# # Path # #

Module_Name = helpers.module_name(__file__)

# Directories
Data = Path("data")
Logs = Data / "logs"
Png = Data / "png"
Csv = Data / "csv"
# Data Tables
# HJ_Strains = str(Csv / "Harvest Journal Strains.csv")  # Future Feature
# PNG Files
Create_PackagePNG = str(Png / "createPackage.png")

# # Other Constants # #

# Package RFID field
Rfid = "WASTE"
# Item field
ItemId = {"INDICA": "25220R", "SATIVA": "25221R",
          "HYBRID": "25222R", "CBD":
          "25223R"}
UoM = "GR"
# Bin
Bin = "REGULATED"


# Timing
Timeout = 3

# Setup
random.seed(time.time())
pyautogui.FAILSAFE = True
# Allow moving mouse to corners to interupt program

# # Logger Setup # #

try:
    LogFile = Logs / f"{Module_Name}.log"
    LogFmt = "%(asctime)s - %(levelname)s - %(message)s"
    logger = logging.getLogger(__name__)  # Get logger with module name
    fileHndl = logging.FileHandler(LogFile)
    # Get file handler  # mode='a' OR mode='w'
    streamHndl = logging.StreamHandler()  # Get stream handler
    formatter = logging.Formatter(LogFmt)  # Get formatter

    # Handle message levels
    logger.setLevel(logging.DEBUG)
    fileHndl.setLevel(logging.DEBUG)
    streamHndl.setLevel(logging.INFO)

    # Set formatter
    fileHndl.setFormatter(formatter)
    streamHndl.setFormatter(formatter)

    # Add file handlers
    logger.addHandler(fileHndl)
    logger.addHandler(streamHndl)
except RuntimeWarning:
    # Attempt to log setup failure
    print(traceback.format_exc())
    print("An exception occured... Logging disabled.")
    # Notify setup failure outside logging
    logging.disable()  # Disable logging after failed setup


def main():
    # Open Nav and 'Harvest Journal'
    harvest_journal.open()

    # Export 'Harvest Journal' and Metrc harvests reports
    to_enter = harvest_journal.calculate_waste_to_enter()

    # Remove all 0.00 values
    to_enter = to_enter.loc[to_enter["Waste"] > 0]
    Session_Entry_Count = len(to_enter["Waste"])

    # Navigate to the first entry
    pyautogui.press("down")  # Unhighlight rows
    pyautogui.press("up", presses=10, interval=0.01)
    harvest_journal.jump_down(50)  # Test

    batch = str()  # Create inital value for batch and last_batch
    successful_entry_count = 0
    repeat_count = 0
    while True:
        last_batch = batch  # Store last batch
        # Load entry into clipboard and return batch
        time.sleep(Timeout)  # Time to load after 'continue'
        batch = harvest_journal.read_active_batch()
        # Finished warning
        if batch == last_batch:
            repeat_count += 1
            # Check if this is the end, or a repeated batch name
            # and ask to quit
            if repeat_count > 0:  # if repeat_count < 2:
                msg = f"{batch} already seen {repeat_count} times. Exit?"
                # Don't quit
                if pyautogui.confirm(msg) == "Cancel":
                    pyautogui.press("down")  # Move down an entry
                    continue
            Unsuccessful_Entry_Count = (Session_Entry_Count
                                        - successful_entry_count)
            msg = ("Waste Entries Not Entered: "
                   f"{Unsuccessful_Entry_Count}")
            logger.info(msg)

            # Which quit message?
            if Unsuccessful_Entry_Count == 0:
                msg = "No more waste values! Finished entering waste."
                logger.info(msg)
                pyautogui.alert(msg)
            elif Unsuccessful_Entry_Count > 0:
                msg = (f"It appears the {Module_Name} module has hit the "
                        "bottom of 'Harvest Journal'")
                logger.info(msg)
                pyautogui.alert(msg)
            if pyautogui.confirm("Enter set_trace()?") == "OK":
                set_trace()
                exit(0)
            else:
                break
        # No indication near end
        else:  # last_batch != batch
            repeat_count = 0

        # ############## Batch exists check ############### #
        try:
            weight = to_enter.loc[batch]["Waste"]
        except KeyError:
            # Finally here?
            # pyautogui.press("esc")  # Exit 'Create Package' window
            time.sleep(1)
            pyautogui.press("down")  # Highlight next entry
            # pyautogui.hotkey("ctrl", "shift", 'c')  # Get row in memory
            continue
        # ############## DataFrame instead of float ############## #
        # (i.e. multiple waste values in batch)
        if not isinstance(weight, float):
            msg = (f"WASTE NOT ENTERED, MULTIPLE WASTE VALUES!\n"
                   f"Failed to create waste package for: {batch}")
            logger.critical(msg)
            # Inspect Issues
            msg += "\nset_trace()?"
            if pyautogui.confirm(msg) == "OK":
                set_trace()

            # pyautogui.press("esc")  # Exit 'Create Package' window
            time.sleep(0.5)
            pyautogui.press("down")  # Highlight next entry
            # pyautogui.hotkey("ctrl", "shift", 'c')  # Get row in memory
            continue
        # ############## End check ############### #

        # Get fields unique to each batch
        item = "25217R"
        # Add Code #:
        # item = to_enter.loc[batch]["Dominancy"]
        # Add Code #

        # # PyAutoGUI Nav # #

        # Click create package #

        # Potential Issue w/ updated exception handling
        try:
            coordinates = pyautogui.locateCenterOnScreen(Create_PackagePNG)
        except TypeError:
            pyautogui.alert("Cannot find 'Create Package' button on screen."
                            "Press 'OK' to exit.")
            exit(-1)
        ht_pyautogui.click_move(coordinates)

        # Enter package info
        pyautogui.run(f"s0.2 w'{Rfid}' s0.1 k'tab' s0.1")  # Package RFID
        pyautogui.run(f"w'{item}' s0.1 k'tab' s0.1")  # Item
        pyautogui.run(f"w'{weight}' p0.1 k'tab' s0.1")  # Weight
        pyautogui.run(f"w'{UoM}' s0.1 k'tab' s0.1")  # Unit of Measurement
        #
        # Add Code #:
        # if ht_pyautogui.wait_for(check_boxPNG):
        #     pyautogui.run(f"k'space' s0.1 k'tab' s0.1")  # Tick waste box
        # else
        pyautogui.press("tab")
        pyautogui.run(f"w'{Bin}' s0.1 k'tab' k'tab' s0.1")  # Bin
        # Add Code #:

        """
        # if pyautogui.confirm("Press enter?") == "Cancel": exit(-1)
        # Submit and wait for previous entry to submit
        pyautogui.run("k'enter' s2.0")
        pyautogui.run("k'enter' s1.0")  # Confirm submission

        logger.info("Created waste package for:\n"
                    f"{Rfid}\n"
                    f"{item}\n"
                    f"{weight}\n"
                    f"{UoM}\n"
                     "Waste\n"
                    f"{Bin}\n")
        """
        pyautogui.press("esc")  # Testing
        # Click create package
        successful_entry_count += 1

        pyautogui.press("down")  # Move to the next entry

    return None


if __name__ == "__main__":
    try:
        main()
    except Exception:
        # Alert user and log
        pyautogui.alert(traceback.format_exc())
        logger.critical(traceback.format_exc())
