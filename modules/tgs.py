#!/usr/bin/env python3

"""TGS Module."""

import string

from modules import helpers


def custom_capwords(Text: str) -> str:
    Strain_Map = helpers.load_json("data/json/strain_map_fix.json")

    if Text in Strain_Map:
        return Strain_Map[Text]
    else:
        return string.capwords(Text)
