#!/usr/bin/env python3

"""A module containing custom PyAutoGUI Functions."""

import time
import random
from typing import Callable

import pyautogui


# Module Constants
Move_Duration = 0.24
Drag_Duration = 0.54
Relative_Movement = (50, 50)
Movement_Range = (255, 255)


def runtime_error() -> None:
    """Raises a RuntimeError. Used as a callable argument since raise
    statements are technically not callable. This may have something
    to do with the difference between an expression and a statement."""
    raise RuntimeError()
    return None


def failsafe_max() -> tuple:
    """Returns a tuple containing the x and y max for the current
    pyautogui.FAILSAFE_POINTS."""
    max_x = 0
    max_y = 0
    for coordinate in pyautogui.FAILSAFE_POINTS:
        x, y = coordinate
        if x > max_x:
            max_x = x
        if y > max_y:
            max_y = y
    return (max_x, max_y)


def will_failsafe_trigger(Movement_Amount: tuple) -> bool:
    """If moving the mouse by Movement_Amount will set off a
    pyautogui.FAILSAFE check, this function returns True. If not, this
    function returns False."""
    Failsafe_Max = failsafe_max()
    Position = pyautogui.position()
    Position_X = Position[0]
    Position_Y = Position[1]

    # Check if moving mouse Movement_Amount will put the mouse in an
    # area of the screen that will trigger PyAutoGUI's FAILSAFE exception
    flag_x = ((Position_X + Movement_Amount[0]) < 1
              or (Position_X + Movement_Amount[0]) >= Failsafe_Max[0])
    flag_y = ((Position_Y + Movement_Amount[1]) < 1
              or (Position_Y + Movement_Amount[1]) >= Failsafe_Max[1])

    if flag_x and flag_y:
        return True
    else:
        return False


def random_coordinates() -> tuple:
    """Gets screen size, then generates random coordinates on the
    screen."""
    random.seed(time.time())
    screensize = pyautogui.size()
    return (random.uniform(0, screensize[0]),
            random.uniform(0, screensize[1]))


def random_relative_coordinates(Range: tuple = Movement_Range) -> tuple:
    """Get random (x, y) coordinates in a given
    range."""
    random.seed(time.time())

    return (random.uniform(Range[0] * -1, Range[0]),
            random.uniform(Range[1] * -1, Range[1]))


def move_random(Range: tuple = Movement_Range) -> None:
    """Moves the mouse a random number of relative pixels based
    on a given range."""
    # Check if movement will trigger failsafe
    while True:
        relative_coordinates = random_relative_coordinates()
        if not will_failsafe_trigger(relative_coordinates):
            break

    pyautogui.moveRel(relative_coordinates,
                      duration=Move_Duration)
    return None


def wait_for(ImageFile: str,
             RelativeCoords: tuple = Relative_Movement,
             Wait: int = 0, Timeout: int = 60,
             Timeout_Command: Callable = runtime_error) -> tuple:
    """Waits for a PNG file to appear on screen, then returns the coordinates."""
    start_time = time.time()
    while True:
        coords = pyautogui.locateCenterOnScreen(ImageFile)
        if coords is not None:
            break
        if time.time() - start_time > Timeout:
            Timeout_Command()
        move_random(RelativeCoords)
        time.sleep(Wait)

    return coords


def wait_click(ImageFile: str, RelativeCoords: tuple = Relative_Movement,
               Wait: int = 0) -> None:
    """Waits for PNG file to appear on screen, then clicks the
    coordinates."""
    pyautogui.click(wait_for(ImageFile, RelativeCoords, Wait))
    return None


def click_move(Coordinates: tuple, RelativeCoords: tuple =
               Relative_Movement) -> None:
    """Clicks the given coordinates, then moves Relative_Movement off the
    coordinates/button."""
    pyautogui.click(Coordinates)
    move_random(RelativeCoords)
    return None


def wait_click_move(ImageFile: str, RelativeCoords: tuple = Relative_Movement,
                    Wait: int = 0) -> None:
    """Waits for PNG file to appear on screen, clicks the coordinates,
    then moves off the button."""
    click_move(wait_for(ImageFile, RelativeCoords, Wait), RelativeCoords)
    return None


def wait_rclick(ImageFile: str, RelativeCoords: tuple = Relative_Movement,
                Wait: int = 0) -> None:
    """Waits for PNG file to appear on screen, then right-clicks the
    coordinates."""
    pyautogui.click(wait_for(ImageFile, RelativeCoords, Wait), button="right")
    return None


def scroll_for(Seconds: float = 10.0, Amount: int = -10,
               Coordinates: tuple = pyautogui.position()) -> None:
    """Continuously scrolls for a given amount of seconds."""
    start_timestamp = time.time()
    while True:
        pyautogui.moveTo(Coordinates)
        pyautogui.scroll(Amount)
        if (time.time() - start_timestamp) > Seconds:
            break
    return None


def snap_fullscreen(Coordinates: tuple) -> None:
    """Wait for window title to appear on screen, and drag it to the top of
    the window to snap to fullscreen."""
    # pyautogui.run(f"g{coordinate[0]},{coordinate[1]} p0.1 d{coordinate[0],0")
    # No drag duration feature, saving code above for future implementation
    pyautogui.moveTo(Coordinates)
    pyautogui.dragTo(Coordinates[0], 0, duration=Drag_Duration)
    return None


def unsnap_fullscreen() -> None:
    """Clicks the top of the screen and drags downward to "unsnap" the
    window out of fullscreen."""
    Screensize = pyautogui.size()
    pyautogui.moveTo(Screensize[0] / 2, 5)
    pyautogui.dragRel((0, 24), duration=Drag_Duration)
    return None
